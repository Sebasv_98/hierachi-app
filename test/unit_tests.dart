import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/employees/employee_manager.dart';
import 'package:hierarchyapp/models/employee.dart';

class MockEmployeeManager implements IEmployeeManager {
  String _companyName;
  String _address;

  @override
  String get getAddress => _address;

  @override
  String get getCompanyName => _companyName;

  @override
  Future<List<EmployeeModel>> getEmployees() async {
    await Future.delayed(Duration(milliseconds: 200));
    this._address = "Test address";
    this._companyName = "Test company name";

    return <EmployeeModel>[
      new EmployeeModel(
          id: "1",
          name: "Employee 1",
          wage: 100,
          imagePath: "assets/patinador.png",
          position: "CEO",
          employeesIds: ["2", "3"]),
      new EmployeeModel(
          id: "2",
          name: "Employee 2",
          wage: 50,
          imagePath: "assets/patinadora.png",
          position: "CTO",
          employeesIds: ["3"]),
      new EmployeeModel(
          id: "3",
          name: "Employee 3",
          wage: 75,
          imagePath: "assets/patinador.png",
          position: "CFO",
          employeesIds: [])
    ];
  }
}

void main() {
  //Put GlobalController in test execution
  void startGlobalController() {
    Get.lazyPut<IEmployeeManager>(() => MockEmployeeManager());
    Get.lazyPut<GlobalController>(
        () => GlobalController(employeeManager: Get.find()));
  }

  //Check Global controller is prepared
  test("init Get Test", () {
    expect(Get.isPrepared<GlobalController>(), false);
    expect(Get.isPrepared<IEmployeeManager>(), false);

    startGlobalController();

    expect(Get.isPrepared<IEmployeeManager>(), true);
    expect(Get.isPrepared<GlobalController>(), true);
  });

  test("Test Global Controller", () async {

    startGlobalController();

    final controller = Get.find<GlobalController>();

    expect(controller.initialized, true);

    //loading data
    expect(controller.loading, true);

    //awaits for company data
    await Future.delayed(Duration(seconds: 2));

    //finish loading data
    expect(controller.loading, false);

    //now the company data is set
    expect(controller.companyName, "Test company name");
    expect(controller.address, "Test address");
  });

  test("setEmployees test", () async {
    startGlobalController();
    final controller = Get.find<GlobalController>();

    //awaits for company data
    await Future.delayed(Duration(seconds: 2));

    controller.setEmployees();
    //expect the employees of the employee 1
    expect(controller.employees[0].name, "Employee 1");
    expect(controller.employees[0].employees[0].name, "Employee 2");
    expect(controller.employees[0].employees[1].name, "Employee 3");

    //expect the employees of the employee 2
    expect(controller.employees[1].name, "Employee 2");
    expect(controller.employees[1].employees[0].name, "Employee 3");

    //employee 3 has not employees
    expect(controller.employees[2].name, "Employee 3");
    expect(controller.employees[2].employees, []);
  });

  test("sortByWageManual test", () async {
    startGlobalController();
    final controller = Get.find<GlobalController>();

    //awaits for company data
    await Future.delayed(Duration(seconds: 2));

    controller.sortByWageManual();

    //order the employees list by wage
    expect(controller.employees[0].name, "Employee 1");
    expect(controller.employees[1].name, "Employee 3");
    expect(controller.employees[2].name, "Employee 2");
  });

  test("onNew test", () async {
    startGlobalController();
    final controller = Get.find<GlobalController>();

    //awaits for company data
    await Future.delayed(Duration(seconds: 2));

    //expect employee 1 is not new
    expect(controller.employees[0].isNew, false);
    //set employee 1 as new
    controller.onNew(controller.employees[0], true);
    //now employee 1 is new
    expect(controller.employees[0].isNew, true);
  });

  test("filterByNew test", () async{
    startGlobalController();
    final controller = Get.find<GlobalController>();
    //awaits for company data
    await Future.delayed(Duration(seconds: 2));

    //expect employee 1 is not new
    expect(controller.employees[0].isNew, false);
    expect(controller.employees[0].name, "Employee 1");
    //set employee 2 as new
    controller.onNew(controller.employees[1], true);
    //filter employees by new
    controller.filterByNew();
    //now the list has size 1
    expect(controller.employees.length, 1);
    //the employee in the list is Employee 2
    expect(controller.employees[0].isNew, true);
    expect(controller.employees[0].name, "Employee 2");
  });
}
