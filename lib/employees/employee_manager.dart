import 'dart:convert';
import 'package:hierarchyapp/models/employee.dart';
import 'package:http/http.dart' as http;

class EmployeeManager implements IEmployeeManager {

  String _companyName;
  String _address;

  @override
  Future<List<EmployeeModel>> getEmployees() {
    final String url =
        "https://raw.githubusercontent.com/sapardo10/content/master/RH.json";
    return http.get(url).then((response) {
      try {
        Map<String, dynamic> jsonResponse = json.decode(response.body);
        if (response.statusCode == 200) {
          _companyName = jsonResponse['company_name'] ?? "";
          _address = jsonResponse['address'] ?? "";
          var employeesJson = EmployeesList.fromJson(jsonResponse['employees']);
          return employeesJson.employees;
        } else {
          return [];
        }
      } catch (error) {
        print(error.toString());
        return [];
      }
    });
  }

  @override
  String get getCompanyName => _companyName;

  @override
  String get getAddress => _address;
}

abstract class IEmployeeManager {
  Future<List<EmployeeModel>> getEmployees();
  String get getCompanyName;
  String get getAddress;
}
