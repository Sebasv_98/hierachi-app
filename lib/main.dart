import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/employees/employee_manager.dart';
import 'package:hierarchyapp/pages/welcome_page.dart';
import 'package:hierarchyapp/utils/size_config.dart';
import 'package:hierarchyapp/utils/colors.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      SizeConfig().init(constraints);
      Get.lazyPut<IEmployeeManager>(() => EmployeeManager());
      Get.put(GlobalController(employeeManager: Get.find()));
      return GetMaterialApp(
        title: 'Hierachy App',
        theme: ThemeData(
          primarySwatch: themeColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: WelcomePage(),
      );
    });
  }
}
