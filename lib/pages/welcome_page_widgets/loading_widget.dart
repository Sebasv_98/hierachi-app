import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/widgets/progress_indicator/jumping_dots.dart';

class LoadingWidget extends StatelessWidget {
  final Color textColor;

  const LoadingWidget({Key key, this.textColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      id: 'employees',
      builder: (controller) {
        return controller.loading
            ? JumpingDotsProgressIndicator(textColor: textColor,)
            : SizedBox();
      },
    );
  }

}