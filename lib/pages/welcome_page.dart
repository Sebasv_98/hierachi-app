import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:hierarchyapp/controllers/welcome_controller.dart';
import 'package:hierarchyapp/pages/welcome_page_widgets/loading_widget.dart';
import 'package:hierarchyapp/utils/size_config.dart';
import 'package:hierarchyapp/utils/colors.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WelcomeController>(
      init: WelcomeController(),
      builder: (controller) => Scaffold(
          backgroundColor: redColor,
          body: Center(
            child: Column(
              children: [
                SizedBox(height: 20 * SizeConfig.heightMultiplier),
                Container(
                    height: 30 * SizeConfig.heightMultiplier,
                    child: Image(image: AssetImage('assets/logo1.png'))),
                Text("Hierachy App",
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Colors.white,
                      fontSize: 30,
                    )),
                Container(
                    margin: EdgeInsets.all(20),
                    child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        elevation: 2.0,
                        color: Colors.white,
                        textColor: redColor,
                        onPressed: controller.start,
                        child: Text("Iniciar",
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold)))),
                LoadingWidget(textColor: Colors.white)
              ],
            ),
          )),
    );
  }
}
