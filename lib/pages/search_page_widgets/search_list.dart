import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:hierarchyapp/widgets/employee_card.dart';

class SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      id: "searchEmployees",
      builder: (controller) {
        return ListView.builder(
          itemBuilder: (context, index) {
            final EmployeeModel employee = controller.employeesResult[index];
            return EmployeeCard(
              employeeModel: employee,
              onSearch: true,
            );
          },
          itemCount: controller.employeesResult.length,
        );
      },
    );
  }
}
