import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';

class SearchTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      id: "searchEmployees",
      builder: (controller) {
        return Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
              color: Color(0xFFF5F8FB),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: TextField(
                    controller: controller.textEditingController,
                    cursorColor: Colors.black,
                    keyboardType: TextInputType.name,
                    onChanged: controller.onInputTextChanged,
                    decoration: new InputDecoration(
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 11, top: 11, right: 15),
                        hintText: "Buscar por nombre"),
                  ),
                ),
                controller.inputText.isNotEmpty
                    ? IconButton(
                        icon: Icon(Icons.cancel),
                        color: Colors.black54,
                        onPressed: () {
                          controller.inputText = "";
                          controller.textEditingController.text = "";
                        })
                    : SizedBox()
              ],
            ));
      },
    );
  }
}
