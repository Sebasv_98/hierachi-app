import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:hierarchyapp/pages/welcome_page_widgets/loading_widget.dart';
import 'package:hierarchyapp/utils/size_config.dart';
import 'package:hierarchyapp/widgets/employee_card.dart';

import '../search_page.dart';

class HomeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
        id: 'employees',
        builder: (controller) {
          if (controller.loading) {
            return Center(child: LoadingWidget(textColor: Colors.black));
          } else {
            return Column(
              children: [
                Container(
                  width: SizeConfig.widthMultiplier * 90,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        controller.isFiltered
                            ? "Nuevos empleados:"
                            : "Empleados:",
                        style: TextStyle(fontFamily: 'Poppins', fontSize: 20),
                      ),
                      Spacer(),
                      controller.isFiltered
                          ? SizedBox()
                          : IconButton(
                              icon: Icon(Icons.search),
                              color: Colors.black,
                              onPressed: () => Get.to(SearchPage(),
                                  transition: Transition.fadeIn)),
                      IconButton(
                          icon: controller.isFiltered
                              ? Icon(Icons.cancel)
                              : Icon(Icons.sort),
                          color: Colors.black,
                          onPressed: controller.isFiltered
                              ? controller.showAllEmployees
                              : controller.showFilterOptions),
                    ],
                  ),
                ),
                Expanded(
                  child: RefreshIndicator(
                    onRefresh: controller.loadCompanyData,
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        final EmployeeModel employee =
                            controller.employees[index];
                        return EmployeeCard(
                            employeeModel: employee,
                            canSeeDetail: !controller.isFiltered);
                      },
                      itemCount: controller.employees.length,
                    ),
                  ),
                ),
              ],
            );
          }
        });
  }
}
