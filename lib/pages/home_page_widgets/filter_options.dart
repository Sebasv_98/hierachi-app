import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/utils/size_config.dart';

class FilterOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      builder: (controller) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                  padding: EdgeInsets.only(top: 12, bottom: 12),
                  child: Center(
                      child: Container(
                    height: 5,
                    width: 55 * SizeConfig.widthMultiplier,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                  ))),
              SizedBox(height: 5),
              FlatButton(
                child: Text(
                  "Ordenar por salario",
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Get.back();
                  //controller.sortByWage();
                  controller.sortByWageManual();
                },
              ),
              FlatButton(
                child: Text("Mostrar Nuevos",
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold,
                    )),
                onPressed: () {
                  Get.back();
                  controller.filterByNew();
                },
              ),
              SizedBox(height: 5)
            ],
          ),
        );
      },
    );
  }
}
