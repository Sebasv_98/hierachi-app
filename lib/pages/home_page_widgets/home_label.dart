import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';

class HomeLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      id: 'companyName',
      builder: (controller) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(controller.companyName.toUpperCase(),
                style: TextStyle(
                    fontFamily: "Poppins",
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20)),
            Text(controller.address,
                style: TextStyle(
                  fontFamily: "Poppins",
                  color: Colors.white,
                ))
          ],
        ),
      ),
    );
  }
}
