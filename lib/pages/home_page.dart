import 'package:flutter/material.dart';
import 'package:hierarchyapp/pages/home_page_widgets/home_label.dart';
import 'package:hierarchyapp/utils/colors.dart';
import 'package:hierarchyapp/utils/size_config.dart';
import 'home_page_widgets/home_list.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(children: <Widget>[
        ClipPath(
          clipper: BackGroundClipper(),
          child: Container(
            height: SizeConfig.heightMultiplier * 30,
            width: SizeConfig.widthMultiplier * 100,
            color: redColor,
          ),
        ),
        Padding(
          padding: new EdgeInsets.only(top: statusBarHeight),
          child: Column(
            children: [
              Container(
                  height: SizeConfig.heightMultiplier * 25,
                  child: HomeLabel()),
              SizedBox(height: SizeConfig.heightMultiplier * 2),
              Expanded(child: HomeList())
            ],
          ),
        ),
      ]),
    );
  }
}

class BackGroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height);
    path.quadraticBezierTo(
        size.width / 4, size.height - 40, size.width / 2, size.height - 20);
    path.quadraticBezierTo(
        3 / 4 * size.width, size.height, size.width, size.height - 30);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
