import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/search_controller.dart';
import 'package:hierarchyapp/pages/search_page_widgets/search_list.dart';
import 'package:hierarchyapp/pages/search_page_widgets/search_text_field.dart';
import 'package:hierarchyapp/utils/colors.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: SearchController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0,
              iconTheme: IconThemeData(color: redColor),
              title: Text("Buscar empleados",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.bold))),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SearchTextField(),
                  Expanded(
                    child: SearchList(),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
