import 'package:get/route_manager.dart';
import 'package:get/state_manager.dart';
import 'package:hierarchyapp/pages/home_page.dart';

class WelcomeController extends GetxController {
  void start() {
    Get.to(HomePage(), transition: Transition.downToUp);
  }
}
