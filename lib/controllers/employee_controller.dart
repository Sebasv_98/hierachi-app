import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:hierarchyapp/widgets/employee_deatil_widget.dart';

class EmployeeController extends GetxController {
  void selectEmployee(EmployeeModel employeeModel) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.vertical(top: Radius.circular(20.0))),
      isScrollControlled: true,
      context: Get.overlayContext,
      builder: (context) {
        return EmployeeDetailWidget(employeeModel: employeeModel);
      },
    );
  }

  void selectEmployeeFromSearch(EmployeeModel employeeModel) {
    Get.back();
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.vertical(top: Radius.circular(20.0))),
      isScrollControlled: true,
      context: Get.overlayContext,
      builder: (context) {
        return EmployeeDetailWidget(employeeModel: employeeModel);
      },
    );
  }
}
