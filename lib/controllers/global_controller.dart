import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/employees/employee_manager.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:hierarchyapp/pages/home_page_widgets/filter_options.dart';

class GlobalController extends GetxController {

  GlobalController({this.employeeManager});

  final IEmployeeManager employeeManager;

  String _companyName = "";
  String _address = "";
  List<EmployeeModel> _employees = [];
  List<EmployeeModel> _employeesGlobal = [];
  List<EmployeeModel> _employeesResult = [];
  bool _loading = true;
  bool _isFiltered = false;
  String _inputText = "";
  TextEditingController _textEditingController = TextEditingController();

  String get companyName => _companyName;
  String get address => _address;
  List<EmployeeModel> get employees => _employees;
  List<EmployeeModel> get employeesResult => _employeesResult;
  bool get loading => _loading;
  bool get isFiltered => _isFiltered;
  String get inputText => _inputText;
  TextEditingController get textEditingController => _textEditingController;
  set inputText(String value) {
    this._inputText = value;
    //search(this._inputText);
    searchManual(this._inputText);
    update(['searchEmployees']);
  }

  @override
  void onInit() {
    super.onInit();
    loadCompanyData();
  }

  Future<void> loadCompanyData() async {
    this._loading = true;
    update(['employees']);
    await Future.delayed(Duration(milliseconds: 800));
    final data = await employeeManager.getEmployees();
    this._companyName = employeeManager.getCompanyName;
    this._address = employeeManager.getAddress;
    this._employees = data;
    this._employeesGlobal = data;
    this._loading = false;
    this._isFiltered = false;
    update(['employees', 'companyName']);
    setEmployees();
  }

  void setEmployees() {
    _employees.forEach((employee) {
      employee.employees = employees
          .where((element) => employee.employeesIds.contains(element.id))
          .toList();
    });
  }

  void onInputTextChanged(String text) {
    this._inputText = text;
    //search(this._inputText);
    searchManual(this._inputText);
    update(['searchEmployees']);
  }

  void search(String name) {
    this._employeesResult = _employeesGlobal
        .where((element) =>
            element.name.toLowerCase().contains(name.toLowerCase()))
        .toList();
  }

  void searchManual(String name) {
    this._employeesResult = [];
    this._employeesGlobal.forEach((element) {
      if (element.name.toLowerCase().contains(name.toLowerCase())) {
        this._employeesResult.add(element);
      }
    });
  }

  void filterByNew() {
    this._employees =
        _employeesGlobal.where((element) => element.isNew).toList();
    this._isFiltered = true;
    update(['employees']);
  }

  void sortByWage() {
    this.employees.sort((a, b) => b.wage.compareTo(a.wage));
    update(['employees']);
  }

  void sortByWageManual() {
    EmployeeModel aux;
    int cont1;
    int cont2;
    for (cont1 = 1; cont1 < employees.length; cont1++) {
      aux = employees[cont1];
      for (cont2 = cont1 - 1;
          cont2 >= 0 && employees[cont2].wage < aux.wage;
          cont2--) {
        employees[cont2 + 1] = employees[cont2];
        employees[cont2] = aux;
      }
    }
    update(['employees']);
  }

  void showFilterOptions() {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0))),
      isScrollControlled: true,
      context: Get.overlayContext,
      builder: (context) {
        return FilterOptions();
      },
    );
  }

  void showAllEmployees() {
    this._employees = this._employeesGlobal;
    this._isFiltered = false;
    update(['employees']);
  }

  void onNew(EmployeeModel employee, bool isNew) {
    employee.isNew = isNew;
    update(['employees']);
  }
}
