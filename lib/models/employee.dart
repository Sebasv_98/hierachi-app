import 'dart:math';

class EmployeeModel {
  final String id;
  final String name;
  final String position;
  final double wage;
  final List<String> employeesIds;
  bool isNew = false;
  List<EmployeeModel> employees;
  String imagePath;

  EmployeeModel(
      {this.id,
      this.name,
      this.position,
      this.wage,
      this.employeesIds,
      this.imagePath});

  factory EmployeeModel.fromJson(Map<String, dynamic> json) {
    final List<String> photos = [
      "assets/patinador.png",
      "assets/patinadora.png",
      "assets/patineta.png",
      "assets/patineta1.png",
    ];
    List<String> employeesIds = [];
    (json['employees'] as List<dynamic>).forEach((element) {
      employeesIds.add(element['id'].toString());
    });
    String getRandomImage() {
      Random random = new Random();
      int randomNumber = random.nextInt(4);
      return photos[randomNumber];
    }
    String imagePath = getRandomImage();
    return new EmployeeModel(
      id: json['id'].toString(),
      name: json['name'],
      position: json['position'],
      wage: json['wage'].toDouble(),
      employeesIds: employeesIds,
      imagePath: imagePath,
    );
  }
}

class EmployeesList {
  final List<EmployeeModel> employees;

  EmployeesList({this.employees});

  factory EmployeesList.fromJson(List<dynamic> parsedJson) {
    List<EmployeeModel> employees = new List<EmployeeModel>();
    employees = parsedJson.map((e) => EmployeeModel.fromJson(e)).toList();
    return new EmployeesList(employees: employees);
  }
}
