import 'package:flutter/material.dart';

class JumpingDot extends AnimatedWidget {
  final String character;
  final Color color;
  JumpingDot({
    Key key,
    Animation<double> animation,
    @required this.character,
    @required this.color,
  }) : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Container(
      height: animation.value,
      child: Text(
        character,
        style: TextStyle(
            color: color,
            fontWeight: FontWeight.bold,
            fontFamily: 'Poppins'),
      ),
    );
  }
}

class JumpingDotsProgressIndicator extends StatefulWidget {
  static const String word = "Cargando";
  final int numberOfDots;
  final Color textColor;
  final double beginTweenValue = 20.0;
  final double endTweenValue = 40.0;

  JumpingDotsProgressIndicator(
      {this.numberOfDots = word.length, this.textColor = Colors.white});

  _JumpingDotsProgressIndicatorState createState() =>
      _JumpingDotsProgressIndicatorState(
          numberOfDots: this.numberOfDots, word: word, textColor: textColor);
}

class _JumpingDotsProgressIndicatorState
    extends State<JumpingDotsProgressIndicator> with TickerProviderStateMixin {
  int numberOfDots;
  String word;
  Color textColor;
  List<AnimationController> controllers = new List<AnimationController>();
  List<Animation<double>> animations = new List<Animation<double>>();
  List<Widget> _widgets = new List<Widget>();

  _JumpingDotsProgressIndicatorState(
      {this.numberOfDots, this.word, this.textColor});

  initState() {
    super.initState();
    for (int i = 0; i < numberOfDots; i++) {
      controllers.add(AnimationController(
          duration: Duration(milliseconds: 150), vsync: this));

      animations.add(
          Tween(begin: widget.beginTweenValue, end: widget.endTweenValue)
              .animate(controllers[i])
                ..addStatusListener((AnimationStatus status) {
                  if (status == AnimationStatus.completed)
                    controllers[i].reverse();
                  if (i == numberOfDots - 1 &&
                      status == AnimationStatus.dismissed) {
                    controllers[0].forward();
                  }
                  if (animations[i].value > widget.endTweenValue / 2 &&
                      i < numberOfDots - 1) {
                    controllers[i + 1].forward();
                  }
                }));

      _widgets.add(Padding(
        padding: EdgeInsets.only(right: 1.0),
        child: JumpingDot(
          animation: animations[i],
          character: word[i],
          color: textColor,
        ),
      ));
    }

    controllers[0].forward();
  }

  Widget build(BuildContext context) {
    return SizedBox(
      height: 100.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _widgets,
      ),
    );
  }

  dispose() {
    for (int i = 0; i < numberOfDots; i++) controllers[i].dispose();
    super.dispose();
  }
}
