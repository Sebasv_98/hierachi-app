import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/employee_controller.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:intl/intl.dart';

class EmployeeCard extends StatelessWidget {
  final EmployeeModel employeeModel;
  final bool onSearch;
  final bool canSeeDetail;

  EmployeeCard({
    Key key,
    this.employeeModel,
    this.onSearch = false,
    this.canSeeDetail = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmployeeController>(
      init: EmployeeController(),
      builder: (controller) {
        return Container(
          margin: const EdgeInsets.symmetric(
            vertical: 10.0,
          ),
          child: GestureDetector(
            onTap: () => canSeeDetail ? onSearch
                ? controller.selectEmployeeFromSearch(employeeModel)
                : controller.selectEmployee(employeeModel) : print("Cannot see Detail"),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: 124.0,
                    margin: new EdgeInsets.only(left: 46.0),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.circular(8.0),
                      boxShadow: <BoxShadow>[
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10.0,
                          offset: new Offset(0.0, 10.0),
                        ),
                      ],
                    ),
                    child: Container(
                      margin: EdgeInsets.fromLTRB(66.0, 16.0, 16.0, 16.0),
                      constraints: BoxConstraints.expand(),
                      child: SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                toBeginningOfSentenceCase(employeeModel.name),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 19.0,
                                    fontFamily: 'Poppins',
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(height: 10),
                              Text("Cargo: ${employeeModel.position}",
                                  style: TextStyle(color: Colors.black54)),
                              SizedBox(height: 10),
                              Text("Salario: ${employeeModel.wage}",
                                  style: TextStyle(color: Colors.black54))
                            ]),
                      ),
                    ),
                  ),
                  Container(
                    height: 90,
                    width: 90,
                    margin: new EdgeInsets.symmetric(vertical: 16.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      image: DecorationImage(
                          image: AssetImage(employeeModel.imagePath),
                          fit: BoxFit.cover),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
