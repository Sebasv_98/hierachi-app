import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hierarchyapp/controllers/global_controller.dart';
import 'package:hierarchyapp/models/employee.dart';
import 'package:hierarchyapp/utils/colors.dart';
import 'package:hierarchyapp/utils/size_config.dart';

class EmployeeDetailWidget extends StatelessWidget {
  final EmployeeModel employeeModel;
  const EmployeeDetailWidget({Key key, this.employeeModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<GlobalController>(
      id: 'employees',
      builder: (controller) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(top: 12, bottom: 12),
                  child: Center(
                      child: Container(
                    height: 5,
                    width: 55 * SizeConfig.widthMultiplier,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                  ))),
              SizedBox(height: 10),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        image: DecorationImage(
                            image: AssetImage(employeeModel.imagePath),
                            fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      employeeModel.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    SizedBox(height: 5),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Flexible(
                          child: Text(
                            "Cargo: ${employeeModel.position}",
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "Salario: ${employeeModel.wage}",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      employeeModel.isNew ? "Es nuevo: Sí" : "Es nuevo: No",
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Empleados:",
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold,
                                fontSize: 15))),
                    Container(
                      height: 30 * SizeConfig.heightMultiplier,
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: employeeModel.employees.length,
                          itemBuilder: (context, index) {
                            final employee = employeeModel.employees[index];
                            return EmployeeCard(employeeModel: employee);
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: OutlineButton(
                          child: new Text(
                              employeeModel.isNew
                                  ? "Desmarcar como nuevo empleado"
                                  : "Marcar como nuevo empleado",
                              style: TextStyle(
                                  color: redColor,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15)),
                          borderSide: BorderSide(color: redColor),
                          onPressed: () => controller.onNew(
                              employeeModel, !employeeModel.isNew),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0))),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class EmployeeCard extends StatelessWidget {
  final EmployeeModel employeeModel;
  const EmployeeCard({Key key, this.employeeModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: Colors.white,
      child: ListTile(
        title: Text(
          employeeModel.name,
          style: TextStyle(
              fontFamily: 'Poppins', fontWeight: FontWeight.bold, fontSize: 15),
        ),
        subtitle: Text(employeeModel.position),
        leading: CircleAvatar(
          radius: 20.0,
          backgroundImage: AssetImage(employeeModel.imagePath),
          backgroundColor: Colors.transparent,
        ),
        onTap: () {
          Navigator.pop(context);
          showModalBottomSheet(
            shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.vertical(top: Radius.circular(20.0))),
            isScrollControlled: true,
            context: context,
            builder: (context) {
              return EmployeeDetailWidget(employeeModel: employeeModel);
            },
          );
        },
      ),
    );
  }
}
