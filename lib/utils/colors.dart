import 'package:flutter/material.dart';

const Color redColor = Color(0xFFFF0032);

const MaterialColor themeColor =
const MaterialColor(0xFFFF0032, const <int, Color>{
  50: Color(0xFFFF0032),
  100: Color(0xFFFF0032),
  200: Color(0xFFFF0032),
  300: Color(0xFFFF0032),
  400: Color(0xFFFF0032),
  500: Color(0xFFFF0032),
  600: Color(0xFFFF0032),
  700: Color(0xFFFF0032),
  800: Color(0xFFFF0032),
  900: Color(0xFFFF0032),
});